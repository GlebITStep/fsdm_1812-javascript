let tasks = [];

document.addEventListener('DOMContentLoaded', function () {
    changeTheme();

    let userJson = localStorage.getItem('user');
    if (userJson) {
        let user = JSON.parse(userJson);
        document.forms.enterForm.hidden = true;
        document.getElementById('welcome').hidden = false;
        document.querySelector('#welcome h1').innerText = user.name;
        document.querySelector('#welcome h3').innerText = user.age;
    }

    let tasksJson = localStorage.getItem('tasks');   
    if (tasksJson) {
        tasks = JSON.parse(tasksJson);
        for (const task of tasks) {
            drawTask(task);
        }
    } 
});

document.forms.todoForm.addEventListener('submit', function() { 
    let todo = {
        id: Date.now(),
        title: document.forms.todoForm.title.value
    };
    drawTask(todo);

    tasks.push(todo);
    console.log(tasks);
    localStorage.setItem('tasks', JSON.stringify(tasks)); 
    
    document.forms.todoForm.reset();
    event.preventDefault();
});

document.querySelector('#tasks').addEventListener('click', function() {
    if (event.target.tagName == 'LI') {
        event.target.classList.add('done');   
    }
    else if (event.target.tagName == 'I') {
        let taskId = event.target.parentElement.dataset.id;
        tasks = tasks.filter(x => x.id != taskId);
        localStorage.setItem('tasks', JSON.stringify(tasks)); 
        event.target.parentElement.remove();
    }
});

document.forms.enterForm.addEventListener('submit', function() {
    let user = {
        name: document.forms.enterForm.name.value,
        age: document.forms.enterForm.age.value
    };
    
    let json = JSON.stringify(user);
    localStorage.setItem('user', json);
    event.preventDefault();
    location.reload();
});

document.querySelector('#lightBtn').addEventListener('click', function () {
    localStorage.setItem('theme', 'Light');
    changeTheme();
});

document.querySelector('#darkBtn').addEventListener('click', function () {
    localStorage.setItem('theme', 'Dark');
    changeTheme();
});

function drawTask(task) {
    let item = document.createElement('li');
    item.innerHTML = '<i>❌</i> ' + task.title;
    item.dataset.id = task.id;
    document.getElementById('tasks').append(item);
}

function changeTheme() {
    if (localStorage.getItem('theme') == 'Dark') {
        document.body.classList.remove('light-theme');
        document.body.classList.add('dark-theme');
    }
    else if (localStorage.getItem('theme') == 'Light') {
        document.body.classList.remove('dark-theme');
        document.body.classList.add('light-theme');
    }
}
