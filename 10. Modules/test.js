export const pi = 3.14;

export function hello() {
    console.log('Hello, world!'); 
}

export function sum(num1, num2) {
    console.log(num1 + num2); 
}

export function alert() {
    console.log('private'); 
}



// export let functions = {
//     pi,
//     hello,
//     sum
// };
