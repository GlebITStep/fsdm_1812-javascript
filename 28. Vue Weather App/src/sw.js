var cacheName = 'WeatherAppPWA-v1';
var appShellFiles = [
    'index.html',
    'bundle.js',
    'assets/preloader.svg',
    // 'assets/placeholder.png',
    'assets/manifest.json',
    'assets/favicon.ico',
    'assets/apple-touch-icon.png',
    'assets/android-chrome-512x512.png',
    'assets/android-chrome-192x192.png',
];

self.addEventListener('install', (e) => {
    console.log('[Service Worker] Install');
    e.waitUntil(
        caches.open(cacheName).then((cache) => {
            console.log('[Service Worker] Caching all: app shell and content');
            return cache.addAll(appShellFiles);
        })
    );
});

self.addEventListener('fetch', (e) => {
    console.log('HERE');
    e.respondWith(
        caches.match(e.request).then((r) => {
            console.log('[Service Worker] Fetching resource: ' + e.request.url);
            return r || fetch(e.request).then((response) => {
                return caches.open(cacheName).then((cache) => {
                    console.log('[Service Worker] Caching new resource: ' + e.request.url);
                    cache.put(e.request, response.clone());
                    return response;
                });
            });
        })
    );
});

