import Vue from 'vue/dist/vue.js';

const weatherApiUrl = 'https://api.openweathermap.org/data/2.5/weather';
const weatherApiKey = '835492c0eab300994ec658dfb16ad305';

function getCoodinatesAsync() {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(position => {
            resolve(position);
        }, error => {
            console.log(error);
            reject(error.message);
        });
    });
}

async function getWeatherByNameAsync(name) {
    let response = await fetch(`${weatherApiUrl}?q=${name}&apikey=${weatherApiKey}&units=metric`);
    if (response.status != 200) throw response;
    return response.json();
}

async function getWeatherByCoordsAsync(lat, lon) {
    let response = await fetch(`${weatherApiUrl}?lat=${lat}&lon=${lon}&apikey=${weatherApiKey}&units=metric`);
    if (response.status != 200) throw response;
    return response.json();
}

async function getCityImage(name) {
    const bingApiUrl = 'https://stepbingsearchapitest.cognitiveservices.azure.com/bing/v7.0/images/search';
    const bingApiKey = '7f789034260442e99ebec663aedc551d';

    let response = await fetch(`${bingApiUrl}?q=${name}`, {
       headers: {
           'Ocp-Apim-Subscription-Key': bingApiKey
       } 
    });
    return response.json();
}

let app = new Vue({
    el: '#app',

    data: {
        cityName: '',
        errorText: '',
        position: null,
        cityList: [],
        currentWeather: null
    },

    methods: {
        onCityFormSubmit: async function() {
            try {
                let response = await getWeatherByNameAsync(this.cityName);
                response.iconSrc = `https://openweathermap.org/img/wn/${response.weather[0].icon}@2x.png`;
                // response.cityImg = (await getCityImage(this.cityName + ' city')).value[0].contentUrl;
                this.cityList.push(response);   
            } catch (error) {
                this.errorText = 'City not found!';
            }
            this.cityName = '';
        },

        onAlertClose: function() {
            this.errorText = '';
        },

        onCityClick: function(name) {
            this.currentWeather = this.cityList.find(x => x.name == name);
        },

        onImgError: function(name) {
            this.cityList.find(x => x.name == name).cityImg = './assets/palceholder.png'; 
        }
    },

    mounted: async function() {
        document.getElementById('loading').remove();

        try {
            this.position = await getCoodinatesAsync();   
            let currentCityWeather = await getWeatherByCoordsAsync(this.position.coords.latitude, this.position.coords.longitude);
            let cityName = currentCityWeather.name;
            currentCityWeather.name = `Current location - ${currentCityWeather.name}`; 
            currentCityWeather.iconSrc = `https://openweathermap.org/img/wn/${currentCityWeather.weather[0].icon}@2x.png`;
            // currentCityWeather.cityImg = (await getCityImage(cityName + ' city')).value[0].contentUrl;
            // console.log(currentCityWeather.cityImg); 
            this.cityList.push(currentCityWeather);
        } catch (error) {
            this.errorText = error;
        }
    }
});

// Vue.set(this.cityList, 0, { name: 'TEST' });
// this.cityList.splice(0, 1, { name: 'TEST' });
// this.cityList[0] = { name: 'TEST' };