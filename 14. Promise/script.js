// async function main() { 
//     try {
//         let results = await Promise.all([
//             loadScriptAsync('one.js'),  
//             loadScriptAsync('two.js'),
//             loadScriptAsync('three.js')
//         ]);

//         for (const result of results) {
//             console.log(result);
//         }
//     } catch (error) {
//         console.error(error); 
//     }
// }
// main();






async function main() { 
    try {
        let result1 = await loadScriptAsync('one.js');
        console.log(result1); 
        one();
        let result2 = await loadScriptAsync('to.js');
        console.log(result2);
        two();
        let result3 = await loadScriptAsync('three.js');
        console.log(result3);
        three();   
    } catch (error) {
        console.error(error); 
    }
}
main();




// Promise.race([
//     loadScriptAsync('one.js'),  
//     loadScriptAsync('two.js'),
//     loadScriptAsync('three.js')
// ]).then(result => {
//     console.log(result); 
// });



// Promise.all([
//     loadScriptAsync('one.js'),  
//     loadScriptAsync('two.js'),
//     loadScriptAsync('three.js')
// ]).then(results => {
//     one();
//     two();
//     three();
//     console.log('All loaded!!!');
//     for (const result of results) {
//         console.log(result); 
//     }
// });



// loadScriptAsync('one.js').then(result => { 
//     one();
//     return loadScriptAsync('two.js');
// }).then(result => { 
//     two();
//     return loadScriptAsync('three.js');
// }).then(result => { 
//     three();
// }).catch(error => {
//     console.error(error); 
// });


// loadScriptAsync('one.js').then(result => one());
// loadScriptAsync('two.js').then(result => two());
// loadScriptAsync('three.js').then(result => three());

// loadScriptAsync('one.js1')
//     .then(result => one())
//     .catch(error => console.error(error));

function loadScriptAsync(src) {
    return new Promise(function(resolve, reject) {

        setTimeout(() => {
            let script = document.createElement('script');
            script.src = src;
            
            script.onload = function() {
                console.log('Script loaded...'); 
                resolve(src);
            };
        
            script.onerror = function() {
                reject(new Error('Not found!'));
            };
        
            document.body.append(script);
        }, 500);
        
    });
}














// let promise = new Promise(function(resolve, reject) {

//     setTimeout(() => {
//         //reject('Error!');
//         resolve('DONE'); 
//     }, 500);
    
// });

// promise
//     .then(result => console.log(result))
//     .catch(error => console.error(error))
//     .finally(() => console.log('FINALLY!'));

    


// promise.then(function(result) {
//     console.log(result); 
// }).catch(function(error) {
//     console.error(error); 
// });
    



// promise.then(
//     result => console.log(result),
//     error => console.log(error) 
// );

// promise.then(function(result) {
//     console.log(result); 
// }, function(error) {
//     console.error(error);
// });



















// let scriptLoaded = new Event('scriptLoaded');
// loadScript('one.js');
// document.addEventListener('scriptLoaded', function() {
//     one();
// });

// loadScript('one.js', function() {
//     one();
//     loadScript('two.js', function() {
//         two();
//         loadScript('three.js', function() {
//             three();
//         });
//     });
// });

// loadScript('one.js', function(error) {
//     if (error) {
//         console.error(error); 
//     } else {
//         one();
//     }
// });

// setTimeout(() => {
//     one(); 
// }, 500);



function loadScript(src, callback) {
    let script = document.createElement('script');
    script.src = src;
    
    script.onload = function() {
        console.log('Script loaded...'); 
        // document.dispatchEvent(scriptLoaded);
        // callback();
    };

    script.onerror = function() {
        // console.log('Not found!'); 
        // callback(new Error('Not found!'));
    };

    document.body.append(script);
}