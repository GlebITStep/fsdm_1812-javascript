let app = new Vue({
    el: '#app',

    // Data
    data: {
        title: 'Hello from VueJS!',
        person: {
            name: 'Imran',
            age: 22
        },
        firstNum: 8,
        secondNum: 9,
        showFullInfo: true
    },

    // Methods
    methods: {
        onButtonClick: function () {
            console.log('button clicked!');
            this.title = 'button clicked!!!';
            this.showFullInfo = !this.showFullInfo;
        }
    }
});

let calc = new Vue({
    el: '#calc',

    data: {
        firstNum: 0,
        secondNum: 0,
        operation: '+'
    },

    methods: {
        onClearClick: function () {
            this.firstNum = 0;
            this.secondNum = 0;
        }
    },

    computed: {
        result: function () {
            if (this.operation == '+')
                return this.firstNum + this.secondNum;
            else if (this.operation == '-')
                return this.firstNum - this.secondNum;
            else if (this.operation == '*')
                return this.firstNum * this.secondNum;
            else if (this.operation == '/')
                return this.firstNum / this.secondNum;

            // return eval(`${this.firstNum} ${this.operation} ${this.secondNum}`);
        }
    }
});

let todolist = new Vue({
    el: '#todolist',

    data: {
        tasks: ['Do my homework', 'Feed my cat'],
        newTask: ''
    },

    methods: {
        onAddClick: function() {
            this.tasks.push(this.newTask);
            this.newTask = '';
        },

        onDeleteClick: function (index) {
            this.tasks.splice(index, 1);
        }

        // onDeleteClick: function(taskName) {
        //     this.tasks = this.tasks.filter(x => x != taskName);
        // }
    }
});
// result: {
//     get: function() {
//         return this.firstNum + this.secondNum;
//     },
//     set: function(value) {
//         this.firstNum = value;
//         this.secondNum = value;
//     }
// }