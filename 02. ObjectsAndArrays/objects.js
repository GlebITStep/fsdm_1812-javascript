function hello() {
    console.log('Hello!'); 
}

let obj = {
    name: "Gleb",
    age: 25,
    say: hello,

    test: function() {
        console.log(this.name); 
    },

    info: function() {
        // console.log('Name: ' + this.name + " Age: " + this.age); 
        console.log(`Name: ${this.name} Age: ${this.age}`); 
    }
};

obj.test();
obj.info();

// obj.isAdmin = true;
// obj['one two'] = 12;

// // delete obj.age;

// console.log(obj.name);
// console.log(obj.age);
// console.log(obj['age']);
// console.log(obj.isAdmin);
// console.log(obj['one two']);
