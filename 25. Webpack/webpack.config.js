const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: '.',
    hot: true
  },
  module: {
      rules: [
          { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
          { test: /\.scss$/, use: [ 'style-loader', 'css-loader', 'sass-loader' ] },
          { test: /\.jpg$/, use: [ 'file-loader' ] },
          { test: /\.xml$/, use: [ 'xml-loader' ] },
          { test: /\.csv$/, use: [ 'csv-loader' ] },
      ]
  }
};