import './style.scss'
import './test.css'

import igor from './igor.jpg'
import jsonData from './data.json'
import xmlData from './data.xml'
import csvData from './data.csv'

import $ from 'jquery'

console.log(jsonData);
console.log(xmlData);
console.log(csvData);

$('body').append(`
    <div style="color:green">Hello</div>
`);

$('#img').attr('src', igor);

console.log('Hello!');
