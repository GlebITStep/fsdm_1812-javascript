// let numbers = [11, 8, 20, 15, 6, 7, 1];
// numbers.sort((x, y) => y - x);
// console.log(numbers);





// let items = [
//     { title: 'Tea', price: 12, discount: 10 },
//     { title: 'Coffee', price: 20, discount: 15 },
//     { title: 'Cola', price: 8, discount: 20 },
//     { title: 'Water', price: 3, discount: 0 },
//     { title: 'Milk', price: 6, discount: 5 }
// ];

// //all items price > 6
// //apply discounts
// //sum
// // function comparePrices(first, second) { //+ 0 -
// //     return first.price - second.price;
// // }

// items.sort((x, y) => x.price - y.price);
// console.log(items);


// let max = items.reduce((result, x) => x.price > result ? result = x.price : result, 0);
// console.log(max);

// let sum = items
//     .filter(x => x.price > 6)
//     .map(x => x.price - (x.price / 100 * x.discount))
//     .reduce((result, x) => result + x, 0);


// let filtred = items.filter(x => x.price > 6);
// console.log(filtred);
// let prices = filtred.map(x => x.price - (x.price / 100 * x.discount));
// console.log(prices);
// let sum = prices.reduce((result, x) => result + x, 0);
// console.log(sum);


// //1
// let filtred = [];
// for (const item of items) {
//     if (item.price > 6) {
//         filtred.push(item); 
//     }
// }

// //2
// for (const item of filtred) {
//     item.price -= item.price / 100 * item.discount;
// }

// //3
// let sum = 0;
// for (const item of filtred) {
//     sum += item.price;
// }

// console.log(sum);













// let test = {
//     x: 10
// };

// const obj = {
//     name: 'Gleb',
//     age: 25, 
//     date: {
//         day: 16,
//         month: 1
//     }
// }
// Object.setPrototypeOf(obj, test);

// for (const key in obj) {
//     if (obj.hasOwnProperty(key)) {       
//         console.log(key + ' ' + obj[key]); 
//     }
// }






// //ENUM
// const Priority = {
//     LOW: 1,
//     MEDIUM: 2,
//     HIGH: 3
// };
// Object.freeze(Priority);
// console.log(Priority.MEDIUM);










// const obj = {
//     name: 'Gleb',
//     age: 25, 
//     date: {
//         day: 16,
//         month: 1
//     }
// }
// // Object.seal(obj);
// // Object.freeze(obj);
// obj.age = 45;
// obj.date.day = 5;
// obj.test = 'test';
// console.log(obj);










// var text = 'hello!';
// hello();







// function getCounter() {
//     let count = 0;
//
//     return function() {
//         return count++;
//     }
// }

// let counter = getCounter(); 
// console.log(counter());
// console.log(counter());
// console.log(counter());

// let counter2 = getCounter();
// console.log(counter2());
// console.log(counter2());
// console.log(counter2());





// let name = 'Imran';
// function test() {
//     let name = 'Gleb';
//     function printName() {
//         let age = 25;
//         console.log(`${name} ${age}`);
//     }
// }







// let name = 'Gleb';

// function printName() {
//     let age = 25;
//     console.log(`${name} ${age}`);
// }

// //printName.[[Enviroment]] = { age: 25, next: { name: 'Gleb', printName: function(){...} } }
// printName();














// function printInfo(surname, salary) {
//     console.log(`${this.name} ${this.age}`);  
//     console.log(`${surname} ${salary}`); 
// }

// let obj1 = {
//     name: 'Gleb',
//     age: 25,
// }

// let obj2 = {
//     name: 'Test',
//     age: 33,
// }

//call apply bind
// printInfo();
// printInfo.call(obj1, 'Skripnikov', 10000);
// printInfo.call(obj2, 'Empty', 0);
// printInfo.apply(obj1, [ 'Skripnikov', 10000 ]);
// printInfo.apply(obj2, [ 'Empty', 0 ]);
// let glebInfo = printInfo.bind(obj1);
// glebInfo(1, 2);
// glebInfo(3, 4);




// let obj = {
//     name: 'Gleb',
//     age: 25,

//     printInfo: function() {
//         setTimeout(() => {
//             console.log(`${this.name} ${this.age}`);  
//         }, 1000);
//     }
// }
// obj.printInfo();

// //Function declaration
// function foo1() {
//     console.log('foo1');
// }

// //Function expression
// var foo2 = function() {
//     console.log('foo2'); 
// }

