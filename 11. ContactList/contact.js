/**
 * Create new contact
 * @param {Name} name 
 * @param {Phone} phone 
 * @param {Email} email 
 * @param {Favorite} favorite 
 */
export function Contact(name = "Untitled", phone = "00000000", email = "", favorite = false) {
    this.id = Date.now();
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.favorite = favorite;
}
