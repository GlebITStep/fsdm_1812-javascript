// document.addEventListener('DOMContentLoaded', function () {
//     changeTheme();
// });

// document.querySelector('#lightBtn').addEventListener('click', function () {
//     document.cookie = "theme=Light; max-age=3600";
//     changeTheme();
// });

// document.querySelector('#darkBtn').addEventListener('click', function () {
//     document.cookie = "theme=Dark";
//     changeTheme();
// });

// function changeTheme() {
//     let theme = document.cookie.split(';');
//     let value = theme[0].split('theme=')[1];
//     console.log(value);

//     if (value == 'Dark') {
//         document.body.classList.remove('light-theme');
//         document.body.classList.add('dark-theme');
//     }
//     else if (value == 'Light') {
//         document.body.classList.remove('dark-theme');
//         document.body.classList.add('light-theme');
//     }
// }










document.addEventListener('DOMContentLoaded', function () {
    changeTheme();
});

document.querySelector('#lightBtn').addEventListener('click', function () {
    setCookie("theme", "Light", {});
    changeTheme();
});

document.querySelector('#darkBtn').addEventListener('click', function () {
    setCookie("theme", "Dark", {});
    changeTheme();
});

function changeTheme() {
    if (getCookie('theme') == 'Dark') {
        document.body.classList.remove('light-theme');
        document.body.classList.add('dark-theme');
    }
    else if (getCookie('theme') == 'Light') {
        document.body.classList.remove('dark-theme');
        document.body.classList.add('light-theme');
    }
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

    options = {
        path: '/',
        // при необходимости добавьте другие значения по умолчанию
        ...options
    };

    // if (options.expires.toUTCString()) {
    //     options.expires = options.expires.toUTCString();
    // }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}