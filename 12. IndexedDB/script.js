let dbConnection = indexedDB.open('contactList', 1);

function getRnd(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function getName(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
 

dbConnection.onsuccess = function() {
    console.log('SUCCESS'); 
    let db = event.target.result;

    // // CREATE
    // let transaction = db.transaction('contacts', 'readwrite');
    // let contacts = transaction.objectStore('contacts');
    // contacts.add({ name: "Gleb", age: 56 });
    // // for (let i = 0; i < 10; i++) {
    // //     contacts.add({ name: getName(getRnd(2, 10)), age: getRnd(5, 100) });
    // // }


    // // READ ALL
    // let transaction = db.transaction('contacts', 'readonly');
    // let contacts = transaction.objectStore('contacts');
    // let request = contacts.getAll();
    // // let request = contacts.getAll(IDBKeyRange.bound(2, 6));
    // request.onsuccess = function() {
    //     console.log(request.result);  
    // }


    // // READ BY KEY
    // let transaction = db.transaction('contacts', 'readonly');
    // let contacts = transaction.objectStore('contacts');
    // let request = contacts.get(8);
    // request.onsuccess = function() {
    //     console.log(request.result);  
    // }


    // // DELETE
    // let transaction = db.transaction('contacts', 'readwrite');
    // let contacts = transaction.objectStore('contacts');
    // let request = contacts.delete(1);
    // request.onsuccess = function() {
    //     console.log(request.result);  
    // }


    // // UPDATE
    // let transaction = db.transaction('contacts', 'readwrite');
    // let contacts = transaction.objectStore('contacts');
    // let request = contacts.get(2);
    // request.onsuccess = function() {
    //     let obj = request.result;  
    //     obj.name = "UPDATED";
    //     contacts.put(obj);
    // }
};

dbConnection.onerror = function() {
    console.log('ERROR'); 
};

dbConnection.onupgradeneeded = function() {
    console.log('UPGRADENEEDED'); 
    let db = event.target.result;
    db.createObjectStore('contacts', { keyPath: 'id', autoIncrement: true });
};






















// let dbConnection = indexedDB.open('contactList', 1);

// dbConnection.onsuccess = function() {
//     console.log('SUCCESS'); 
//     let db = event.target.result;

//     let transaction = db.transaction('contacts', 'readwrite');
//     let contacts = transaction.objectStore('contacts');
//     // contacts.add({ name: "Gleb", age: 25, id: Date.now() }, 1);
//     // contacts.add({ title: "Tea", price: 12.6 }, 2);
//     // contacts.add(['one', 'two', 'three'], 3);
//     // contacts.add(['one', 'two', 'three'], { title: "Tea", price: 12.6 });
// };

// dbConnection.onerror = function() {
//     console.log('ERROR'); 
// };

// dbConnection.onupgradeneeded = function() {
//     console.log('UPGRADENEEDED'); 
//     let db = event.target.result;
//     db.createObjectStore('contacts');
// };