"use strict"

console.log("Hello, world!");

var x; //undefined
console.log(x);
console.log(typeof(x));

x = 5; //number
console.log(x);
console.log(typeof(x));

x = 'Gleb'; //string
console.log(x);
console.log(typeof(x));

x = true; //boolean
console.log(x);
console.log(typeof(x));

x = [1, 2, 3]; //object (Array)
console.log(x);
console.log(typeof(x));

x = { name: 'Gleb', age: 25 }; //object (Object)
console.log(x);
console.log(typeof(x));

x = null; //object (null)
console.log(x);
console.log(typeof(x));

x = Infinity; //number (Infinity)
console.log(x);
console.log(typeof(x));

x = NaN; //number (NaN)
console.log(x);
console.log(typeof(x));

