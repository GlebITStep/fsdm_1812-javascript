const apiKey = '835492c0eab300994ec658dfb16ad305';

function renderCity(data) {
    let templateHtml = $('#cityElem').html();
    let template = Handlebars.compile(templateHtml);
    let result = template(data);
    $('#cities').append(result);
}

function addMap(elem, coords) {   
    new google.maps.Map(elem, {
        center: { lat: coords.lat, lng: coords.lon },
        zoom: 10,
        disableDefaultUI: true
    });
}

$('#citySearch').submit(async function() {
    event.preventDefault();
    let name = $('#cityName').val();

    try {
        let data = await $.get(`https://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${apiKey}`).promise();
        console.log(data);   
        renderCity(data);
        addMap($('.map:last')[0], data.coord);
    } catch (error) {
        console.log('City not found!!!'); 
    }
});

$('#cities').on('click', '.header', function() {
    $('#cities .description').not($(this).next()).slideUp();
    $('#cities .header').not(this).find('i').removeClass('fa-minus').addClass('fa-plus');
    $(this).next().slideToggle();
    $(this).find('i').toggleClass('fa-plus').toggleClass('fa-minus');
});


$('#box').resizable().draggable();


var openPhotoSwipe = function() {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // build items array
    var items = [];
    $('img').each(function() {
        items.push({
            src: $(this).attr('src'),
            w: window.innerWidth,
            h: window.innerHeight
        });
    });
    // var items = [
    //     {
    //         src: 'https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_b.jpg',
    //         w: 964,
    //         h: 1024
    //     },
    //     {
    //         src: 'https://farm7.staticflickr.com/6175/6176698785_7dee72237e_b.jpg',
    //         w: 1024,
    //         h: 683
    //     }
    // ];
    
    // define options (if needed)
    var options = {
             // history & focus options are disabled on CodePen        
        history: false,
        focus: false,

        showAnimationDuration: 0,
        hideAnimationDuration: 0
        
    };
    
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};

$('img').click(function() {
    openPhotoSwipe();
});

tinymce.init({
    selector: '#mytextarea',
    height: 500,
    menubar: false,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tiny.cloud/css/codepen.min.css'
    ]
  });
  












// $('#citySearch').submit(function() {
//     event.preventDefault();
//     let name = $('#cityName').val();
    
//     let templateHtml = $('#cityElem').html();
//     $('#cities').append(templateHtml);
// });

// $('#cities .header').click(function() {
//     $('#cities .description').not($(this).next()).slideUp();
//     $('#cities .header').not(this).find('i').removeClass('fa-minus').addClass('fa-plus');
//     $(this).next().slideToggle();
//     $(this).find('i').toggleClass('fa-plus').toggleClass('fa-minus');
// });




















// $('#cities .header').click(function() {
//     // $(this).next().slideToggle();
//     // $(this).nextAll().slideToggle();
//     // $(this).nextUntil('.header').slideToggle();

//     // $(this).prev();
//     // $(this).prevAll();
//     // $(this).prevUntil('.header');

//     // $(this).parent().slideToggle();
//     // console.log($(this).parents());
//     // console.log($(this).parentsUntil('body'));

//     // console.log($('#cities').children());
//     // console.log($('#cities').find('h1'));
// });