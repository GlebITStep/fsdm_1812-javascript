import { MyTime } from './my-time.js'
import { MyCarousel } from './my-carousel.js'

customElements.define('my-time', MyTime);
customElements.define('my-carousel', MyCarousel);