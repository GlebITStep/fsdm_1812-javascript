export class MyCarousel extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        this.root = this.attachShadow({ mode: 'open' });
        this.root.innerHTML = template;

        this.images = this.root.querySelector('slot[name="images"]').assignedElements();
        this.images[0].style.display = 'block';
        this.currentImage = 0;
        
        this.root.getElementById('prev').addEventListener('click', () => {
            if (this.currentImage >= 1) {
                this.images[this.currentImage].style.display = 'none';
                this.currentImage--;
                this.images[this.currentImage].style.display = 'block';   
            }
        });

        this.root.getElementById('next').addEventListener('click', () => {
            if (this.currentImage < this.images.length - 1) {
                this.images[this.currentImage].style.display = 'none';
                this.currentImage++;
                this.images[this.currentImage].style.display = 'block';   
            }
        });
    }    
}

const template = `
<style>
    ::slotted(img) {
        width: 100%;
        object-fit: cover;
        display: none;
    }

    button {
        background-color: green;
    }
</style>

<div>
    <div style="color: red;">
        <slot name="title"></slot>
    </div>
    <button id="prev">⏮</button>
    <button id="next">⏭</button>
    <div>
        <slot name="images"></slot>  
    </div>
</div>
`;