export class MyTime extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        this.hour12 = JSON.parse(this.getAttribute('hour12'));

        this.root = this.attachShadow({ mode: 'open' });
        // this.root.innerHTML = document.getElementById('myTimeTemplate').innerHTML;
        
        this.root.innerHTML = `
            <p>Current time:</p>
            <p id="time">Loading...</p>
        `;
    }

    disconnectedCallback() {
        clearInterval(this.timerId);
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name == 'hour12') {
            this.hour12 = JSON.parse(newValue);
            clearInterval(this.timerId);
            this.startTimer();   
        }
    }

    startTimer() {
        this.timerId = setInterval(() => {
            this.root.getElementById('time').innerText = new Date().toLocaleTimeString('en-US', { hour12: this.hour12 });
        }, 1000); 
    }

    static get observedAttributes() {
        return ['hour12']
    }
    
}















// export class MyTime extends HTMLElement {

//     constructor() {
//         super();
//     }

//     connectedCallback() {
//         this.hour12 = JSON.parse(this.getAttribute('hour12'));

//         this.root = this.attachShadow({ mode: 'open' });
//     }

//     disconnectedCallback() {
//         clearInterval(this.timerId);
//     }

//     attributeChangedCallback(name, oldValue, newValue) {
//         if (name == 'hour12') {
//             this.hour12 = JSON.parse(newValue);
//             clearInterval(this.timerId);
//             this.startTimer();   
//         }
//     }

//     startTimer() {
//         this.timerId = setInterval(() => {
//             this.root.innerHTML = new Date().toLocaleTimeString('en-US', { hour12: this.hour12 });
//         }, 1000); 
//     }

//     static get observedAttributes() {
//         return ['hour12']
//     }
    
// }
