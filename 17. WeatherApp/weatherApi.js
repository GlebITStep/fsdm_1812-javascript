const apiKey = '835492c0eab300994ec658dfb16ad305';
const apiUrl = 'https://api.openweathermap.org/data/2.5/';

export async function getCities() {
    let response = await fetch('./cities.json');
    return response.json();
}

export async function getWeatherDataByCityName(cityName) {
    let response = await fetch(`${apiUrl}weather?q=${cityName}&appid=${apiKey}`);
    return response.json();
}