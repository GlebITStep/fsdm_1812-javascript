import * as weatherApi from './weatherApi.js';

let searchForm = document.forms.citySearch;

async function onPageLoad() {
    // let cities = await weatherApi.getCities();
    // renderCitiesList(cities);
}

async function onTextInput() {
    let cities = await weatherApi.getCities();
    renderCitiesList(cities);
}

async function onSearch() {
    event.preventDefault();
    let cityName = searchForm.city.value;
    let weatherData = await weatherApi.getWeatherDataByCityName(cityName);
    console.log(weatherData); 
}

function renderCitiesList(cities) {
    let datalist = document.getElementById('cities');
    datalist.innerHTML = '';
    for (const city of cities) {
        let cityOption = document.createElement('option');
        cityOption.value = city.name;
        datalist.append(cityOption);
    }
}

searchForm.addEventListener('submit', onSearch);
document.addEventListener('DOMContentLoaded', onPageLoad);
searchForm.city.addEventListener('input', onTextInput);