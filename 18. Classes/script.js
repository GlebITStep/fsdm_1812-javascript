class Person {

    test = 'test';
    _protected = 1;

    constructor(name, surname, age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    get fullname() {
        return `${this.name} ${this.surname}`;
    }

    set fullname(value) {
        this.name = value.split(' ')[0];
        this.surname = value.split(' ')[1];
    } 

    showInfo() {
        console.log(`${this.name} | ${this.age}`);
    }

    // static createEmptyPerson() {
    //     return new Person('Empty', 'Empty', 0);
    // }
}
Person.createEmptyPerson = function() {
    return new Person('Empty', 'Empty', 0);
}

class Student extends Person {

    constructor(name, surname, age, group) {
        super(name, surname, age);
        this.group = group;
    }

    showInfo() { 
        super.showInfo();
        console.log(`${this.group}`);
    }

}

let student = new Student('Gleb', 'Skripnikov', 25, 'FSDE_1812');

console.log(student.fullname);
student.fullname = 'Qleb Skripnikov';

student.showInfo();

let p = Person.createEmptyPerson();
console.log(p);




// function hello() {
//     console.log(this.name); 
// }
// hello.call(p);



// let person = new Person('Gleb', 25);
// console.log(person);










// let personProto = {
//     showInfo: function() {
//         console.log(`${this.name} | ${this.age}`);
//     }
// }

// function Person(name, age) {
//     this.name = name;
//     this.age = age;
// }
// Person.prototype = personProto;

// function Student(name, age, group) {
//     Person.call(this, name, age);
//     this.group = group;
// }
// Student.prototype = Person.prototype;

// let student = new Student('Gleb', 25, 'FSDE_1812');
// student.showInfo();
// console.log(student);








// function Student(group) {
//     this.group = group;
// }
// Student.prototype = Person;

// let student = new Student('FSDE_1812');
// console.log(student);



// let obj = {};
// Person.call(obj, 'Gleb', 25);
// obj.__proto__.constructor = Person;
// console.log(obj);


// let person1 = new Person('Gleb', 25);
// let person2 = { name: 'Gleb', age: 25 };
// console.log(person1.constructor);
// console.log(person2.constructor);















// let person = {
//     name: 'Gleb',
//     age: 25,

//     showInfo: function() {
//         console.log(`${this.name} | ${this.age}`); 
//     }
// };

// let student = {
//     group: 'FSDE_1812',
//     // __proto__: person,

//     showInfo: function() {
//         this.__proto__.showInfo();
//         console.log(`${this.group}`); 
//     }
// }

// student.__proto__ = person;

// student.showInfo();
// // console.log(student.name);
