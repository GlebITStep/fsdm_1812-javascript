// var mymap = L.map('map').setView([51.505, -0.09], 13);

// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
// 	maxZoom: 19,
// 	id: 'mapbox.streets',
// 	accessToken: 'your.mapbox.access.token'
// }).addTo(mymap);


















let map;
let marker;
let infowindow;

map = new google.maps.Map(document.getElementById('map'), {
    center: { lat: 40.3926601, lng: 49.8685246 },
    zoom: 4
});

// marker = new google.maps.Marker({
//     position: { lat: 40.3926601, lng: 49.8685246 },
//     map: map,
//     draggable: true,
//     // icon: '../TEST/WEBCOMPONENTS/igor.jpg'
// });



map.addListener('click', async data => {

    if (infowindow) {
        infowindow.open(null); 
    }

    let lat = data.latLng.lat();
    let lng = data.latLng.lng();
    let response = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=835492c0eab300994ec658dfb16ad305&units=metric`);
    let weather = await response.json();
    console.log(weather);

    infowindow = new google.maps.InfoWindow({
        content: `
            <h1>${weather.name}</h1>
            <p>${weather.main.temp}</p>
        `,
        position: { lat, lng }
    });

    infowindow.open(map);
});

var flightPlanCoordinates = [
    {lat: 37.772, lng: -122.214},
    {lat: 21.291, lng: -157.821},
    {lat: -18.142, lng: 178.431},
    {lat: -27.467, lng: 153.027}
  ];
  var flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 10
  });

  flightPath.setMap(map);

// map.data.loadGeoJson('data.json');

// // let lat = 40.3926601;
// // setInterval(() => {
// //     marker.setPosition({ lat: lat+=0.01, lng: 49.8685246 });
// //     map.panTo({ lat: lat, lng: 49.8685246 });
// // }, 100);
